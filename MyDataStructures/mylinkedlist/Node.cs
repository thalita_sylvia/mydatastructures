﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataStructures.mylinkedlist
{
    class Node
    {
        private int value;
        private Node next;

        public Node(int value)
        {
            this.value = value;
            next = null;
        }
        public Node(int value, Node next)
        {
            this.value = value;
            this.next = next;
        }
        public int getValue()
        {
            return value;
        }
        public Node getNext()
        {
            return next;
        }
        public void setNext(Node next)
        {
            this.next = next;
        }

    }
}
