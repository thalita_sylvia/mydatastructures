﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataStructures.mystack
{
    public class MyStack
    {
        Node top;
        int countStack;

        public MyStack()//Creates an empty stack
        {
            top = null;
            countStack = 0;
        }

        public bool isEmpty()
        {
            return top == null;
        }

        public void push(int value)//Pushes an value to the top of the stack
        {
            if (isEmpty())
            {
                top = new Node(value, null);
            }
            else
            {
                Node temp = new Node(value, top);
                top = temp;
            }
            countStack++;
        }

        public int pop()//Returns and removes the value that was inserted most recently
        {
            int temp = 0;

            if(!isEmpty())
            {
                temp = top.getValue();
                top = top.getNext();
            }
            return temp;
        }
    }
}
