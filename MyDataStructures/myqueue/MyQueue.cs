﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataStructures.myqueue
{
    public class MyQueue
    {
        Node head;
        Node tail;
        int countQueue;

        public MyQueue()
        {
            countQueue = 0;
            head = null;           
        }

        public bool isEmpty()
        {
            return head == null;
        }

        public int peekHead()//Returns the head element without removing it 
        {
            return head.getValue();
        }

        public void insert(int value)//Adds an element on the tail
        {
            if (isEmpty())
            {
                head = new Node(value);
                tail = head;
            }
            else
            {
                tail.setNext(new Node(value));
                tail = tail.getNext();
            }
            countQueue++;
        }

        public int remove()//Removes the current head in the Queue and returns its value (FIFO)
        {
            int temp = 0;

            if(!isEmpty())
            {
                temp = head.getValue();
                head = head.getNext();
                countQueue--;
            }
            return temp;
        }
    }
}
