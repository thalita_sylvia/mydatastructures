﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataStructures.mydoublylinkedlist
{
    class Node
    {
        private int value;
        private Node prev;
        private Node next;

        public Node(int value)
        {
            this.value = value;
            prev = null;
            next = null;
        }
        public Node(int value, Node prev, Node next)
        {
            this.value = value;
            this.prev = prev;
            this.next = next;
        }
        public int getValue()
        {
            return value;
        }
        public Node getPrev()
        {
            return prev;
        }
        public void setPrev(Node prev)
        {
            this.prev = prev;
        }
        public Node getNext()
        {
            return next;
        }
        public void setNext(Node next)
        {
            this.next = next;
        }
    }
}
