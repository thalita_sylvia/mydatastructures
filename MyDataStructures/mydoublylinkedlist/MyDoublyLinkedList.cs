﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataStructures.mydoublylinkedlist
{
    public class MyDoublyLinkedList
    {
        private Node head;
        private int listCount;


        public MyDoublyLinkedList()//Creates a default list
        {
            listCount = 0;
            head = null;
        }

        public MyDoublyLinkedList(int[] array)//Creates a list from an array of numbers
        {
            listCount = 0;
            for (int i = 0; i < array.Length; i++)
            {
                addLast(array[i]);
            }
        }

        public bool isEmpty()//Verifies if there is at least one element on the list 
        {
            return head == null; 
        }

        public void addFirst(int value)//Adds an element to the head of the list
        {
            if (isEmpty())
            {
                head = new Node(value);
            }
            else
            {
                Node temp = new Node(value, null, head);
                head = temp;
            }
            listCount++;
        }

        public void addLast(int value)//Adds an element after all non null elements
        {
            if (isEmpty())
            {
                head = new Node(value);
            }
            else
            {
                Node current = head;
                //starting at the head node, crawl to the end of the list 
                while (current.getNext() != null)
                {
                    current = current.getNext();
                }
                //the last node's next reference set to a new node
                current.setNext(new Node(value, current, null));    
            }
            listCount++;//increment the number of elements in the list
        }

        public int getFirst()//Returns the first element in the list
        {
            int temp = 0;

            if (!isEmpty())
            {
                temp = head.getValue();
            }
            return temp;
        }

        public int getLast()//Returns the last element in the list
        {
            int temp = 0;

            if (isEmpty())
            {
                Node current = head;

                while(current.getNext()!= null)
                {
                    current = current.getNext();
                }
                temp = current.getValue();
            }
            return temp;
        }

        public int removeFirst()//Removes the first element from the list and returns it
        {
            int temp = 0;

            if (!isEmpty())
            {
                temp = head.getValue();
                head = head.getNext();
                head.setPrev(null);
                listCount--;
            }
            return temp;
        }

        public int removeLast()//Removes the last element from the list and returns it
        {
            int temp = 0;

            if (!isEmpty())
            {
                Node current = head;

                while(current.getNext()!= null)
                {
                    current = current.getNext();
                }
                temp = current.getValue();

                current.getPrev().setNext(null);
            }
            return temp;
        }
    }
}
